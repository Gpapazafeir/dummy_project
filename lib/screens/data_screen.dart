import 'package:flutter/material.dart';

class DataScreen extends StatelessWidget {
  const DataScreen({Key? key}) : super(key: key);

  static const routeName = '/dataScreen';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('MyCredentials'),
        backgroundColor: Theme.of(context).primaryColor,
      ),
    );
  }
}
