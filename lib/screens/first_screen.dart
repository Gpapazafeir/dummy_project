import 'package:flutter/material.dart';
import 'package:play_app/widgets/text_input.dart';
import '../screens/data_screen.dart';

class FirstScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).appBarTheme.backgroundColor,
        title: Text('My first App'),
        actions: [
          IconButton(
            onPressed: () {
              Navigator.of(context).pushNamed(DataScreen.routeName);
            },
            icon: Icon(Icons.more_rounded),
          ),
        ],
      ),
      body: TextInput(),
    );
  }
}
