import 'package:flutter/material.dart';
import 'package:play_app/providers/credentials.dart';
import 'package:provider/provider.dart';

import 'package:play_app/screens/data_screen.dart';
import 'package:play_app/screens/first_screen.dart';
import './providers/credentials.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<UserCredential>(
            create: (ctx) => UserCredential())
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primaryColor: Colors.red,
          primarySwatch: Colors.blue,
          appBarTheme: AppBarTheme(
            backgroundColor: Color.fromRGBO(40, 50, 60, 20.0),
          ),
          textTheme: TextTheme(
            headline1: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        home: FirstScreen(),
        routes: {DataScreen.routeName: (ctx) => DataScreen()},
      ),
    );
  }
}
