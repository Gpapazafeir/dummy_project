import 'package:flutter/material.dart';

import 'package:provider/provider.dart';
import '../providers/credentials.dart';

class TextInput extends StatefulWidget {
  @override
  
  _TextInputState createState() => _TextInputState();
}

class _TextInputState extends State<TextInput> {
 
  final _formKey = GlobalKey<FormState>();

  String email = '';
  String username = '';

  void _trySubmit() {
    final isValid = _formKey.currentState!.validate();
    FocusScope.of(context).unfocus();

    if (isValid) {
      _formKey.currentState!.save();
    }
 Provider.of<UserCredential>(context).addToList(authData)
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        margin: EdgeInsets.all(15),
        child: SingleChildScrollView(
          child: Container(
            width: double.infinity,
            margin: EdgeInsets.all(10),
            decoration: BoxDecoration(
                border: Border.all(
                    color: Theme.of(context).primaryColor, width: 7)),
            padding: EdgeInsets.all(10),
            child: Form(
                key: _formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    TextFormField(
                      decoration: const InputDecoration(
                        hintText: 'Enter your email',
                        hintStyle: TextStyle(color: Colors.grey),
                      ),
                      validator: (value) {
                        if (value == null ||
                            value.isEmpty ||
                            !value.contains('@')) {
                          //elegxos krhthriwn
                          return 'Please Enter a Valid email Address';
                        }
                        return null;
                      },
                      keyboardType: TextInputType.emailAddress,
                      onSaved: (value) {
                        email = value.toString();
                        print(email);
                      },
                    ),
                    TextFormField(
                      decoration: InputDecoration(hintText: 'Username'),
                      validator: (value) {
                        if (value == null || value.length < 4) {
                          return 'plz enter a bigger name';
                        }
                        return null;
                      },
                      onSaved: (value) {
                        username = value.toString();
                        print(username);
                      },
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 10),
                      child: ElevatedButton(
                        child: const Text('Submit'),
                        onPressed: _trySubmit,
                      ),
                    )
                  ],
                )),
          ),
        ),
      ),
    );
  }
}
