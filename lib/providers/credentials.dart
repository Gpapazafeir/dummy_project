import 'package:flutter/material.dart';

class Credential {
  final String username;
  final String email;

  Credential(
    @required this.username,
    @required this.email,
  );
}

class UserCredential with ChangeNotifier {
  List<Credential> _items = [];

  List<Credential> get item {
    return [..._items];
  }

  void addToList(List<Credential> authData) {
    _items.insert(0, Credential('Giorgos', 'test@test.com'));
  }
}
